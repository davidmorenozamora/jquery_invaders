
//document ready
$(function(){

    //control de la musica
    let audio = new Audio('sounds/background-music.mp3');
    
    /* cada td esta composat de #fila_columna */
    const Game = $('#App');
    let field = [];
    let deltaTime = 1000;
    const fieldRows = 20;
    const fieldCols = 10;
    let gameStarted = false;
    let score = 0;
    let player = {
        position: {
            col: Math.round(fieldCols / 2),
            row: fieldRows - 1
        }, 
        shoots: []
    }
    let enemies = [];

    let lastInput;

    loadMenu();

    function checkCollision(actualElement){

        //aixo s'ha de fer amb un for perque el return functioni
        if(field[actualElement.row - 1][actualElement.col] == "e"){
            return 'enemy';
        }

    }


    /* MOVIMENT DELS DISPARS */
    setInterval(() => {
        if (gameStarted) {
            player.shoots.forEach((element, index) => {

                //per no sobrecarregar l'array eliminem si han sortit del taulell
                if (element.row < 0) {
                    player.shoots.splice(index, 1);
                }

                field[element.row][element.col] = 0;
                $('#' + element.row + '_' + element.col).css('background-color', 'unset');
                element.row--;

                field[element.row][element.col] = "s";
                if (checkCollision(element) == 'enemy') {
                    //hem de eliminar el dispar i l'enemic
                    field[element.row][element.col] = 0;
                    player.shoots.splice(index, 1);

                    //hem de eliminar l'enemic que esta a la posicio element.row element.col
                    enemies.forEach((enemy, row) => {
                        let indexToDelete = enemy.findIndex(function(e){
                            return e.row == element.row - 1 && e.col == element.col;
                        });
                        if (indexToDelete != -1) {
                            field[enemies[row][indexToDelete].row][enemies[row][indexToDelete].col] = 0;
                            $('#' + enemies[row][indexToDelete].row + '_' + enemies[row][indexToDelete].col).css('background', 'transparent');
                            enemies[row].splice(indexToDelete, 1);

                            //augmentem la puntuacio
                            score = score + 100;
                            $('#score').text(score);

                        }

                    });
                }
                else{
                    $('#' + element.row + '_' + element.col).css('background-color', 'green');
                }

            });

        }
    }, 200);

    /* MOVIMENT ENEMICS */
    let tilesMoved = Math.round(fieldRows / 2);
    setInterval(() => {
        if (gameStarted) {
            score++;
            $('#score').text(score);
            /* NECESSITEM CREAR ENEMICS DE FORMA ALEATORIA, FARAN SPAWN CADA MIG TAULER MOGUT DELS ENEMICS ACTUALS, PER TANT COM A MAXIM SEMPRE TINDREM 2 ROWS DE ENEMICS */
            if (tilesMoved == Math.round(fieldRows / 2)) {
                spawnEnemies();
                tilesMoved = 0;

                //augmentem la dificultat
                if (deltaTime > 50) {
            
                    deltaTime -= 10;

                }
        
            }

            tilesMoved++;
            
            /* NECESSITEM FER QUE ES MOGUIN ELS ENEMICS */
            enemies.forEach((element, index) => {
                let deleteRow = false;
                element.forEach((e, i) => {
                    if (e.row > fieldRows) {
                        deleteRow = true;
                    }
                    if(e.row > 0 && e.row < fieldRows){
                        field[e.row][e.col] = 0;
                    }
                    $('#' + e.row + '_' + e.col).css('background-color', 'unset');
                    enemies[index][i].row++;
                    $('#' + e.row + '_' + e.col).css('background-color', 'red');

                    if(e.row > 0 && e.row < fieldRows){
                        field[e.row][e.col] = "e";
                    }


                    //si es troben amb el jugador game over
                    if (e.row > 0 && e.row < fieldRows && field[e.row + 1][e.col] == "p") {
                        
                        gameStarted = false;

                        gameOver();

                    }

                });
                if (deleteRow || typeof(element) == 'undefined' || element.length <= 0) {
                    enemies.splice(index, 1);
                }

            });

        }
    }, deltaTime);

    function spawnEnemies(){

        /* generarem un nombre random que simbolitzara el nombre de enemics */
        let enemyNumber = Math.floor(Math.random() * (fieldCols - 1));
        let enemiesTiles = [];

        //ara els assignarem a una tile aleatoria
        while (enemiesTiles.length != enemyNumber) {
            
            let rand = Math.floor(Math.random() * (fieldCols - 1));
            //necessitem assignar les caselles a l'array
            if (!enemiesTiles.includes(rand)) {
                enemiesTiles.push(rand);
            }

        }

        enemiesTiles.forEach((element, index) => {
            enemiesTiles[index] = {
                row : 0,
                col : element
            }

            //També els imprimirem
            $('#' + 0 + '_' + element).css('background-color', 'red');
            field[0][element] = "e";

        });


        //els guardem a la linia que toca
        enemies.push(enemiesTiles);

    }

    /* PLAYER MOVEMENT */
    $(window).keypress(function(e) {
        if (gameStarted) {
            lastInput = e.which;
            let hasMoved = false;
            let tmpPosition = {col : player.position.col, row : player.position.row};

            /*CHULETA 
            W = 119 i 87
            A = 97 i 65
            S = 115 i 83
            D = 100 i 68
            SPACE = 32
            */

            // per mirar el valor dels input this.console.log(e.which);
            if (lastInput == 97 || lastInput == 65) {
                //moure dreta
                if (tmpPosition.col != 0) {
                    hasMoved = true;
                    tmpPosition.col--;
                }

            }
            else if (lastInput == 100 || lastInput == 68) {
                //moure esquerra
                if (tmpPosition.col != (fieldCols - 1)) {
                    tmpPosition.col++;
                    hasMoved = true;
                }

            }
            else if(lastInput == 32){
                //disparar
                shoot();

            }

            if(hasMoved){

                hasMoved = false;
                movePlayer(tmpPosition);

            }

        }


        function movePlayer(to){

            // Fem el moviment visual
            $('#' + player.position.row + '_' + player.position.col).css('background-color', 'unset');
            $('#' + to.row + '_' + to.col).css('background-color', 'blue');

            // Movem el jugador tant de la seva propia variable com del taulell imaginari que utilitzem
            field[player.position.row][player.position.col] = 0;
            field[to.row][to.col] = "p";
            
            player.position = to;

        }

        function shoot(){

            let tmpShot = {row: player.position.row - 1, col: player.position.col};
            field[player.position.row - 1][player.position.col] = "s";
            
            player.shoots.push(tmpShot);

            $('#' + tmpShot.row + '_' + tmpShot.col).css('background-color', 'green');

        }

        
    });


    /* Functions that insert html */

    function loadGame(){
        loadField();
        loadPlayer();

        function loadPlayer(){
            $('#' + player.position.row + '_' + player.position.col).css('background-color', 'blue')
            field[player.position.row][player.position.col] = 'p';
        }

        function loadField(){
            let htmlInsert = "";

            htmlInsert += '<div style="position:absolute; top: 20px; right: 20px;"><h3 style="color: yellow;">Score: <span id="score">0</span></h3></div>';

            htmlInsert += '<h1 class="pt-2">JQUERY INVADERS</h1>';

            htmlInsert += '<div class="table-responsive"><table class="table"><tbody>';

            for (let i = 0; i < fieldRows; i++) {
                field[i] = [];
                htmlInsert+= '<tr>';
                for (let x = 0; x < fieldCols; x++) {
                    field[i][x] = 0;
                    htmlInsert += '<td style="border: 0;" id="' + i + '_' + x + '"></td>';
                }
                htmlInsert += '</tr>';
            }

            htmlInsert += '</tbody></table></div>';

            htmlInsert += '<button id="start-game" class="btn btn-block btn-primary">START GAME</button>';
            htmlInsert += '<button id="stop-game" class="btn btn-block btn-danger">PAUSE GAME</button>';

            Game.html(htmlInsert);
        }
    }

    function loadMenu(){

        htmlInsert = "";
        htmlInsert += '<h1>Jquery Invaders</h1>'
        htmlInsert += '<div class="d-flex w-100 h-100 justify-content-center align-items-center flex-column mt-5">';
        htmlInsert += '<a href="#" id="new-game" style="text-decoration: none; color: #fff;">';
        htmlInsert += '<div style="background-image: url(img/start_game.jpg); text-align: center; width: 250px; background-size: cover; background-position: center center; height: 150px; display:flex; align-items: center; padding: 10px 10px 10px 10px; ">';
        htmlInsert += '</div>';
        htmlInsert += '</a>';
        htmlInsert += '<div style="width: 250px; text-align: center; background-size: cover; background-position: center center; height: 150px; display:flex; align-items: center; padding: 10px 10px 10px 10px; ">'
        htmlInsert += '<a href="#" id="options" style="text-decoration: none; color: #fff;">'
        htmlInsert += '<img src="img/options.png" class="img-fluid">';
        htmlInsert += '</a>';

        Game.html(htmlInsert);

    }

    function loadOptions() {
        //tindrem musica i dificultat
        htmlInsert = "";
        htmlInsert += '<h1>Jquery Invaders</h1>'
        htmlInsert += '<div class="d-flex w-100 h-100 justify-content-center align-items-center flex-column mt-5">';
        htmlInsert += '<div class="d-flex w-100 h-100 justify-content-center align-items-center mt-5">';
        htmlInsert += '<div id="music-on">';
        htmlInsert += '<h3>Music On</h3>';
        htmlInsert += '<div style="width: 250px; height: 250px; margin-right: 20px; text-align: center; background-image: url(img/music_on.gif); background-size: cover; background-position: center center; display:flex; align-items: center; padding: 10px 10px 10px 10px; "></div>';
        htmlInsert += '</div>';
        htmlInsert += '<div id="music-off">';
        htmlInsert += '<h3>Music off</h3>';
        htmlInsert += '<div style="width: 250px; height: 250px; text-align: center; background-image: url(img/music_off.gif); background-size: cover; background-position: center center; display:flex; align-items: center; padding: 10px 10px 10px 10px; "></div>';
        htmlInsert += '</div>';
        htmlInsert += '</div>';
        htmlInsert += '<div class="d-flex w-100 h-100 justify-content-center align-items-center mt-5">';
        htmlInsert += '<a href="#" id="difficulty" style="text-decoration: none; color: #fff;"><h1>Difficulty</h1></a>';
        htmlInsert += '</div>';
        htmlInsert += '</div>';

        Game.html(htmlInsert);

    }

    function loadDifficultyMenu(){

        htmlInsert = "";
        htmlInsert += '<h1>Jquery Invaders</h1>'
        htmlInsert += '<div class="d-flex w-100 h-100 justify-content-center align-items-center mt-5">';
        htmlInsert += '<div class="set-difficulty" data-difficulty="easy">';
        htmlInsert += '<h3>Easy</h3>';
        htmlInsert += '<img style="width: 250px; height: 250px;" src="img/medium.jpg">';
        htmlInsert += '</div>'
        htmlInsert += '<div class="set-difficulty" data-difficulty="medium" style="margin-left: 20px; margin-right: 20px;">';
        htmlInsert += '<h3>Medium</h3>';
        htmlInsert += '<img style="width: 250px; height: 250px;" src="img/easy.jpg">';
        htmlInsert += '</div>'
        htmlInsert += '<div class="set-difficulty" data-difficulty="hardcore">';
        htmlInsert += '<h3>Hardcore</h3>';
        htmlInsert += '<img style="width: 250px; height: 250px;" src="img/hardcore.jpg">';
        htmlInsert += '</div>'
        htmlInsert += '</div>';
        Game.html(htmlInsert);

    }


    function gameOver(){

        htmlInsert = "";
        htmlInsert += '<div style="flex-direction: column; color: orange;background-color: rgba(0,0,0,0.5); width: 100%; height: 100%; position: absolute; top: 0; left: 0; z-index: 999; display:flex;justify-content:center; align-items: center;">';
        htmlInsert += '<h1>GAME OVER</h1>';
        htmlInsert += '<h3>Your score is: ' + score + '</h3>';
        htmlInsert += '<button id="try_again" class="btn btn-dark">Try Again?</button>';
        htmlInsert += '</div>';

        Game.html(htmlInsert);

    }


    /* Click events */

    $("body").on('click','#start-game', function(e){
        e.preventDefault();
        gameStarted = true;
    });

    $("body").on('click', '#stop-game', function(e){
        e.preventDefault();
        gameStarted = false;
    });

    $('body').on('click',"#reload", function(e){
        e.preventDefault();
        if(confirm('Estas segur? S\'eliminarà tot el procés')){
            loadMenu();
        }
    });

    $("body").on('click','#new-game', function(e){
        e.preventDefault();
        loadGame();
    });

    $("body").on('click','#options', function(e){
        e.preventDefault();
        loadOptions();
    });

    $("body").on('click', '#music-on', function(e){
        e.preventDefault();
        audio.play();
    });

    $("body").on('click', '#music-off', function(e){
        e.preventDefault();
        audio.pause();
    });

    $('body').on('click', '#difficulty', function(e){
        e.preventDefault();
        loadDifficultyMenu();
    });

    $('body').on('click', '#try_again', function(){
        location.reload();
    });

    $('body').on('click', '.set-difficulty', function(e){
        switch($(this).data('difficulty')){
            case 'easy':
                deltaTime = 1000;
                break;
            case 'medium':
                deltaTime = 500;
                break;
            case 'hardcore':
                deltaTime = 250;
                break;
        }

        alert('Difficulty set to ' + $(this).data('difficulty'));

    });

    /* Audio ended */
    audio.addEventListener('ended', function(){
        this.currentTime = 0;
        this.play()
    }, false);


});